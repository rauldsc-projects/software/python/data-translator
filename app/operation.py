import statistics

class Operator:
    def __init__(self):
        self.hola = "mundo"

    def list_to_int(self, data):
        for x in range(0,len(data)):
            if not isinstance(data[x],int):
                data[x] = int(data[x])
        return data

    def add(self, data):
        numbers = self.list_to_int(data)
        return sum(numbers)

    def mean(self, data):
        numbers = self.list_to_int(data)
        return statistics.mean(numbers)
    
    def stdesviation(self, data):
        numbers = self.list_to_int(data)
        return statistics.stdev(numbers)