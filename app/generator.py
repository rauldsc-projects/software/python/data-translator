"Datasets Generator class"
from faker import Faker
import pandas
fake = Faker("es_ES")

class Generator(Faker):
    #Data Generator class
    def __init__(self):
        super().__init__()


    def generate_list(self,category,quantity):
        """
        Returns a randomly generated list
        """
        target_list = []
        for _ in range(quantity):
            target_list.append(getattr(fake, category)())
        return target_list

    def generate_csv(self, categories, rows, filename):
        """
        Returns a randomly generated csv file
        """
        df = self.generate_df(categories,rows)
        df.to_csv(f"./app/examples_datasets/randomly_generated/{filename}.csv")
        
    def generate_json(self, categories, rows, filename):
        """
        Returns a randomly generated json file
        """
        df = self.generate_df(categories,rows)
        df.to_json(f"./app/examples_datasets/randomly_generated/{filename}.json")

    def generate_df(self, categories, rows):
        """
        Returns a randomly generated dataframe
        """
        dictionary = {}
        for category in categories:
            dictionary[category] = self.generate_list(category,int(rows))
        return pandas.DataFrame.from_dict(dictionary)
        
    def generate_dict(self, categories, rows):
        """
        Returns a randomly generated dictionary
        """
        df = self.generate_df(categories,rows)
        df.to_dict()
    
    def generate_excel(self, categories, rows, filename):
        """
        Returns a randomly generated excel file
        """
        df = self.generate_df(categories,rows)
        df.to_excel(f"./app/examples_datasets/randomly_generated/{filename}.xlsx")